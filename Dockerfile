FROM alpine
ARG VERSION

ADD https://github.com/hetznercloud/cli/releases/download/${VERSION}/hcloud-linux-amd64.tar.gz /tmp/
RUN tar xzf /tmp/hcloud-linux-amd64.tar.gz -C /tmp && \
    cp /tmp/hcloud /usr/local/bin

ENTRYPOINT [ "/usr/local/bin/hcloud" ]
CMD [ "--help" ]
