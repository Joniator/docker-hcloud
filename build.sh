#!/usr/bin/env bash
IMAGE="joniator/hcloud"

if [ -z "$1" ]; then
  VERSION=$(curl https://api.github.com/repos/hetznercloud/cli/releases/latest | jq ".tag_name" | cut -d"\"" -f2)
else
  VERSION="$1"
fi

function docker_tag_exists() {
  curl --silent -f -lSL https://index.docker.io/v1/repositories/$1/tags/$2 > /dev/null
}

if docker_tag_exists $IMAGE $VERSION; then
  echo "$IMAGE:$VERSION exists, exiting without building"
  exit 0
else
  echo "$IMAGE:$VERSION does not exist, building"
  DOCKER_LATEST_TAG="$IMAGE:latest"
  DOCKER_VERSION_TAG="$IMAGE:$VERSION"
  
  docker build --pull --build-arg VERSION=$VERSION -t "$DOCKER_LATEST_TAG" -t "$DOCKER_VERSION_TAG" .
  docker push "$DOCKER_LATEST_TAG"
  docker push "$DOCKER_VERSION_TAG"
fi
